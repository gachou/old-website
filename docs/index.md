# Documentation

## For users

* [Getting started!](./getting-started.md)


## For developers 

* [Architecture](./architecture.html)
* [Source-Code on Gitlab](https://gitlab.com/gachou)
  * [gachou-api](https://gitlab.com/gachou/gachou-api): This project contains the TypeScript interface-definitions for internal and external APIs
  * [gachou-backend](https://gitlab.com/gachou/gachou-backend): This project implements the backend (implemented with express and TypeScript)
  * [gachou-frontend](https://gitlab.com/gachou/gachou-frontend): This project implements the frontend (implemente with VueJS and TypeScript)
  * [gachou-docker-image](https://gitlab.com/gachou/gachou-docker-image): This project builds a docker-image for testing Gachou.


## Trivia

* The name is derived from the Japanese word 画帖 ([pronounced](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet): ɡadʑo) for "picture album"
* Thanks to
  * Daphne, for helping me out while searching for a japanese name for the project
  * [jisho.org](https://jisho.org) - The english-japanese dictionary
  * [www.clker.com](https://www.clker.com) - Public-domain clipart archive
  * [Kanji-to-romaji converter](http://easypronunciation.com/en/japanese-kanji-to-romaji-converter)
# Getting started!

::: warning PRE ALPHA VERSION

gachou is still in a pre-alpha stage. Its features are **not** complete, and **you may loose your data.** But it can already be tested. 

If you want to know, when it is in a usable stage, you can subscribe to this list:

<Interested />

:::


## Using the docker-image

The simplest way to test Gachou is the docker image:

```
docker pull registry.gitlab.com/gachou/gachou-docker-image:latest
docker run --name gachou -d -p 8090:8080 registry.gitlab.com/gachou/gachou-docker-image:latest
```

Then go to `http://localhost:8090`.

::: warning SECURITY WARNING
The docker-image is for testing only, and it is far from secure. The daemon may run as root within the image,
unless you specify the volume `/gachou/media`. In that case it will run as the user owning this directory.
:::

## Using npm

Without the docker-image, backend and frontend have to be installed separately. You can install the `gachou-backend` via npm 

```bash
npm install -g @gachou/gachou-backend
```

Run `gachou-backend --help` to see instructions on how to execute it.

The frontend can be downloaded as build-artifact of the gitlab-repository at [https://gitlab.com/gachou/gachou-frontend](https://gitlab.com/gachou/gachou-frontend). Download the build artifact, extract the zip-file to any folder and add the location as argument to the `gachou-backend` program.


## Debugging and developing

If you want to dig into Gachou, you can clone and run the backend

```bash
git clone https://gitlab.com/gachou/gachou-backend
cd gachou-backend
yarn install
# Start the backend in debugging mode on port 5000
yarn serve 
```

And the run the webpack-dev-server of the frontend using the awesome `vue-cli` tool.

```bash
git clone https://gitlab.com/gachou/gachou-frontend
cd gachou-frontend
yarn install
# Start the webpack-dev-server on port 8080 with a proxy to the backend on port 5000
yarn serve
```





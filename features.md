# Features

::: warning PRE ALPHA VERSION

gachou is still in a pre-alpha stage. Its features are **not** complete, and **you may loose your data.** But it can already be tested. 

If you want to know, when it is in a usable stage, you can subscribe to this list:

<Interested />

:::

With the current version, you can to the following:

## Upload of media-files

It is possible to upload photos and videos in the browser. 

Supported file types

* Videos: Everything that is recognized as video by [exiftool](https://www.sno.phy.queensu.ca/~phil/exiftool/) and can be converted by [ffmpeg](https://www.ffmpeg.org/).
* Images: `png` and `jpg`.

All videos will be converted to `mp4`, because this is the only format that can store XMP-Metadata.

## Display of metadata and filtering

The following metadata will be extracted and displayed for each image

* the creation date and time
* the length of the video (in seconds)

It is possible to filter images

* by year
* by month
* by MIME-type

## Display modes

* In the `Files`-mode, only thumbnails are displayed
* In the `Details`-mode, one image is displayed in a large box. Thumbnails are still display in a row at the bottom
* In `Fullscreen`-mode, one image is display full-screen, without anything other distracting.

## Keyboard and touch control

In `Details`- and `Fullscreen`-mode you can use a swipe-gesture to switch to the next and previouse file.
On a desktop computer, you can use cursor keys.

## Performance considerations

In the `Details`-mode, focused images are scaled on-the-fly the a size similar (slightly larger) than the size of the viewport. That way, the large original image does not have to be loaded. Videos are loading the original version, since they are more expensive to scale.

Known sizes (like the thumbnail size) are created directly after uploading a file, so they are available at once (videos included).

# Planned and possible features

* Tagging
* Detection of similar images

# Further possible tools 

* Import-tool (bulk import of videos and photos from camera SD-card)
* Mobile app to synchronize smartphone-galleries with warau (low-prio)
* Crawler for initial bulk-import of photos on a hard disc
* Combined app based on Electron, for using on a single computer



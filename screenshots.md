# Screenshots

Here are some screens for you to get an impression. It is still early in development a lot is going to change.

## The "files" view

This view just contains a list of thumbnails for the files found in the current search.
Searching (filtering) is possible for "year", "month", "mime-type". But in the end, this should also include tags like "location", "people", "event".

![The "files" view](screenshots/files.png)

## The "details" view

This view shows one large image and thumbnails at the bottom. Use cursor keys or swipe gestures to move back and forth.

![The "details" view](screenshots/details.png)

## The "fullscreen" view

Click on the detail-image and you go to fullscreen. 

![The "details" view](screenshots/fullscreen.png)


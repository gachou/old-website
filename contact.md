# Help wanted!

I am doing most of this work for myself, for my own family fotos and videos. But this might become a big thing and I don't want to do everything on my own. 

* Partly, because it's more fun to do things together and meet like-minded people.
* Partly, because I am not very good at every thing (like UX design), but I still want to create a greate product.

## How can you help

### Frontend development and/or design

If you are good at designing frontends, I would gladly take you advice for improvements.
If you are good at implementing it as well (in Vue.js). Cool stuff. 

* [Contact me](contact.md), if you are interested in helping out.


# Help wanted!

I am doing most of this work for myself, for my own family fotos and videos. But this might become a big thing and I don't want to do everything on my own. 

* Partly, because it's more fun to do things together and meet like-minded people.
* Partly, because I am not very good at every thing (like UX design), but I still want to create a greate product.


## How can you help

<div class="image-box-left">

<img class="theme-image" src="jobs/interaction-designers.png" />

::: tip UX designer

Do you have a knack for frontends? Do you know what the user needs, how she interacts with the software and how the frontend can be designed to fill her needs?

Do you think the current frontend is ugly and unusable? Do you have better ideas?

Well, I love good user interfaces, but I have a hard time designing them. The current frontend is more like a proof-of-concept. I'd love to hear about your thoughts and ideas.

[Subscribe](#subscribe)

:::

</div>

<div class="image-box-right">

<img class="theme-image" src="jobs/1393580373.png" />

::: tip Frontend developer

Do you like implementing frontends in Vue.js? You like to write tests (both unit and end-to-end?). Are you able to seperate you code into clean components?
Do you write clean CSS?

Look at the [frontend-code](https://gitlab.com/gachou/gachou-frontend), you'll see that I have neither been writing tests nor being very strict with my code structure. 
That would need to change. If you think, the only way is to start over again, fine...

I could do this myself, but my resources are finite and there are other things to do as well.
You won't be alone, when you sign up for this job. I'll do my part there as well.

[Subscribe](#subscribe)

:::

</div>

<div class="image-box-left">

<img class="theme-image" src="jobs/network-server.png" />


::: warning Backend developer

The [backend](https://gitlab.com/gachou/gachou-backend/) is probably the only part of the project that is well tested and well planned. I need to write it all down, but I think, I have created a good starting point for an extendable project.

But there is so much to do. Authentication, metadata-extraction, face-recognition, searching for similar images, EU-GDPR compliance, synchronisation between instances.

I can't possibly do all of them on my own. Well, give me enough time and I can, but who wants to wait that long...

If you love TypeScript and you have fun working in the hidden backend, go ahead

[Subscribe](#subscribe)

:::

</div>

<div class="image-box-right">

<img class="theme-image" src="jobs/ausis-pencil-and-note-pad.png" />

::: danger Technical writer

I love to read good documentation. I also like to write documentation. But I am neither a native english speaker, nor do I have a professional or academic background in writing.
I am sure there are enough people out there that could at least help out by reviewing and lectoring the documentation. Or by asking questions and writing documentation as well.

What documentation, you ask...? There will be documentation, for sure. And when there is, there will be the need to improve it.

[Subscribe](#subscribe)

:::

</div>

<div class="image-box-left">

<img class="theme-image" src="jobs/giraffe-maler.png" />

::: tip Artist

If you have knack for drawing pictures, you might want to help out as well. A good logo (maybe even a **better name**), cartoon-images for this web-site. Funny images for the documentation.
All this is something that I am definitely not good at.

[Subscribe](#subscribe)

:::

</div>

## Subscribe


Interested in getting involved? Then please subscribe to this mailinglist. I will reach out to you, when your e-mail is confirmed.
It may still take a while to really get started, but at least I know that I won't be alone by then.

::: warning No pay!
I should probably mention that all those are no real jobs. **You are not going to be paid.** Sign up only, if you think this is an interesting project and you want to make it happen.


<InterestedInHelping />

:::


----

**Images**


[https://openclipart.org/detail/81337/interaction-designers](https://openclipart.org/detail/81337/interaction-designers)
[https://openclipart.org/detail/191397/computer-smartphone-and-tablet](https://openclipart.org/detail/191397/computer-smartphone-and-tablet)
[https://openclipart.org/detail/1697/pencil-and-note-pad](https://openclipart.org/detail/1697/pencil-and-note-pad)
[https://openclipart.org/detail/36565/tango-network-server](https://openclipart.org/detail/36565/tango-network-server)
[https://openclipart.org/detail/182022/giraffe-painter](https://openclipart.org/detail/182022/giraffe-painter)
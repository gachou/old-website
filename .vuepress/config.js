const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");

module.exports = {
  title: "画帳 - Gachou",
  dest: "public",
  repo: "https://gitlab.com/gachou/gachou.org",
  description: "Gachou - your picture album",
  head: [["link", { rel: "icon", href: "favicon.ico" }]],
  chainWebpack: (config, isServer) => {
    config.plugin("copy-artwork").use(CopyPlugin, [
      [
        {
          // Resolve artwork directory via an included file
          from: path.dirname(
            require.resolve("@gachou/gachou-artwork/dist/gachou-logo.svg")
          ),
          to: config.outDir
        }
      ]
    ]);
  },
  themeConfig: {
    editLinks: true,
    repo: "https://gitlab.com/gachou/gachou.gitlab.io",
    nav: [
      { text: "Home", link: "/" },
      { text: "Features", link: "/features.html" },
      { text: "Documentation", link: "/docs/index.html" },
      { text: "Help wanted!", link: "/help-wanted.html" }
    ],
    search: true,
    sidebar: {
      '/docs/': [
        '',     /* /foo/ */
        'getting-started',  /* /foo/one.html */
        'architecture'   /* /foo/two.html */
      ],
      // fallback
      '/': [
        '',       
        'features', 
        'screenshots',
        'help-wanted'    
      ]  
    }
  }
};

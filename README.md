---
home: true
heroImage: /gachou-logo.svg
actionText: Get Started →
actionLink: /docs/getting-started.md
features:
- title: Manage media
  details: Gachou is a web-based tool for managing images and videos. You can upload media-files and tag them. You can filter your files by date and media type or by tags
- title: Self hosted
  details: Gachou is Open Source, licensed under the MIT License. You can install it via npm or use the provided docker-image
- title: Extendable architecture
  details: Gachou has a clean design with internal APIs for metadata-extraction, storage, indexing and more. The frontend is a Vue application that uses a REST-service in the backend.
footer: MIT Licensed | Copyright © 2018-present Nils Knappmeier
---
